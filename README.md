**Actividad: Métricas arquitectónicas - EMI203 (VERSION FINAL TAREA)**

Índice de contenidos

[[_TOC_]]

---

En esta actividad se diseña e implementa una arquitecura de software del tipo monolítica de microkernel cuyo propósito es analizar un repositorio de software escrito en Java y obtener métricas como el grado de abstracción, inestabilidad y distancia de la secuencia principal.

## Modelo C4

### Diagrama a nivel de sistema

![](doc/images/structurizr-SystemContext-001.png)
![](doc/images/structurizr-SystemContext-001-key.png)

### Diagrama a nivel de contenedores

![](doc/images/structurizr-Container-001.png)
![](doc/images/structurizr-Container-001-key.png)

### Diagrama a nivel de componentes

![](doc/images/structurizr-Component-001.png)
![](doc/images/structurizr-Component-001-key.png)


## Instalación

1.  Clonar el repositorio en la máquina de destino usando: \
    `git clone git@gitlab.com:DiegoPinillaU/metricas-arquitectonicas-emi203.git`

2.  Instalar [Java Development Kit](https://www.oracle.com/cl/java/technologies/downloads/#java17) >= 17 y [Maven](https://maven.apache.org/install.html) >= 3.9.5, desde los enlaces provistos. Asegúrese de que el binario mvn se encuentre en el PATH de su sistema.

3. Desde una línea de comandos, ubíquese en el directorio del repositorio y ejecute: \
    `mvn package` 

## Uso
La aplicación generada debe ser ejecutada de la siguiente forma \
    `java -jar target/miscaner.jar ga={true|false} in={true|false} ds={true|false} {carpeta-del-código-fuente}`

Donde los argumentos `ga`, `in` y `ds` pueden tomar valores `true` o `false`, dependiendo de las métricas que necesite el usuario y `carpeta-del-código-fuente` corresponde a la ruta de un proyecto escrito en Java a analizar.


## Autor
Diego Pinilla U. - Estudiante del magíster de ingeniería informática (MII) de la Universidad de La Frontera.
