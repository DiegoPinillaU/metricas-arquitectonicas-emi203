# ADR-0. Usar lenguaje DSL para representar el modelo C4.

## Situación
Aceptado.

## Contexto
Existe una variedad de herramientas de modelado, algunas usando interfaces gráficas, otras en la nube, otras mediante código.

## Decisión
Se decide usar el lenguaje DSL de Structurizr para representar el modelo C4 debido a su poca curva de aprendizaje y reutilización de elementos basado en código. De igual forma, es posible renderizar los diagramas usando distintos medios.

## Consecuencias
Si bien carece de una visualización instantánea o WYSIWYG, es una opción que contribuye de forma eficiente a la calidad de la documentación.

## Cumplimiento
Se genera un documento con extension .dsl en el directorio doc/.

## Notas
Autor: Diego Pinilla U. d.pinilla03@ufromail.cl <br />
Día de publicación: 25-11-2023 <br />
Última actualización: 25-11-2023


# ADR-1. Plugins usan el patrón de diseño de adaptador.

## Situación
Aceptado.

## Contexto
Es necesario optar por un patrón de diseño estructural que compartan todos los plugins

## Decisión
La mejor opción evaluada en este caso corresponde al patrón adaptador, ya que permite la interaccion de objetos (en este caso plugins) con el microkernel.

## Consecuencias
En caso de escalar, todos los plugins adicionales deben implementar una interfaz adaptadora que permita conectarlos al microkernel.

## Cumplimiento
Se genera una interfaz "Plugable" en el paquete cl.ufro.plugins.

## Notas
Autor: Diego Pinilla U. d.pinilla03@ufromail.cl <br />
Día de publicación: 25-11-2023 <br />
Última actualización: 25-11-2023


