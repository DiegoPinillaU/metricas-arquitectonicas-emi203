workspace {

    model {
        user = person "Usuario" "Ejecuta el programa en la línea de comandos"
        softwareSystem = softwareSystem "Escáner de métricas" {
            user -> this "Usa desde la línea de comandos"
            app = container "Aplicación CLI" {
                plugin_ga = component "Plugin grado de abstracción" "Relación entre los artefactos abstractos (clases abstractas, interfaces, etc.) y los artefactos concretos (clases de implementacion)"
                plugin_in = component "Plugin inestabilidad" "Es la relación entre el acoplamiento eferente y la suma del acoplamiento eferente y aferente"
                plugin_dsp = component "Plugin distacia de la secuencia principal" "La distancia a la secuencia principal planea una relación ideal entre la abstracción y la inestabilidad"
                rep_explorer = component "Explorador de repositorio" "Recorre el repositorio y extrae el información de los archivos de código fuente"
                exploration_results = component "Resultados de exploración" "Resumen con el análisis de tipado de los archivos" {
                    rep_explorer -> this "Genera"
                }

                core = component "Core (microkernel)" {
                    plugin_ga -> this "Se carga en"
                    plugin_in -> this "Se carga en"
                    plugin_dsp -> this "Se carga en"
                    core -> exploration_results "Provee a los plugins"
                }
                
                entry_point = component "Punto de entrada del programa" "Obtiene y valida los parámetros ingresados por el usuario, y carga los componentes principales en base a la selección del usuario" {
                    user -> this "Ejecuta el programa"
                    this -> plugin_ga "Inicia basado en entrada"
                    this -> plugin_in "Inicia basado en entrada"
                    this -> plugin_dsp "Inicia basado en entrada"
                    this -> core "Inicia"
                }
            }
        }
    }

    views {
        systemContext softwareSystem {
            include *
            autolayout lr
        }

        container softwareSystem {
            include *
            autolayout lr
        }
        
        component app {
            include *
            autolayout lr
        }

        theme default
    }

}