package cl.ufro;

import java.nio.file.Paths;
import cl.ufro.microkernel.Core;
import cl.ufro.plugins.AbstractionLevelPlugin;
import cl.ufro.plugins.InstabilityLevelPlugin;
import cl.ufro.plugins.MainSequenceDistancePlugin;
import cl.ufro.plugins.Plugable;

public class Main {
    public static void main(String[] args) {
        if (args.length != 4){
            System.out.println("Formato de esperado: java -jar miscaner ga={true|false} in={true|false} ds={true|false} {ruta_al_repositorio}");
            return;
        }
        
        boolean gaOption = Boolean.parseBoolean(args[0].split("=")[1]);
        boolean inOption = Boolean.parseBoolean(args[1].split("=")[1]);
        boolean dsOption = Boolean.parseBoolean(args[2].split("=")[1]);
        String sourceFolder = args[3];

        System.out.println("Resumen de entradas provistas:");
        System.out.printf("\tga=%b\n", gaOption);
        System.out.printf("\tin=%b\n", inOption);
        System.out.printf("\tds=%b\n", dsOption);
        System.out.printf("\truta al repositorio=%s\n", sourceFolder);

        Core core = new Core();
        core.startParsingRepository(Paths.get(sourceFolder));

        if (gaOption){
            Plugable gaPlugin = new AbstractionLevelPlugin();
            core.analyze(gaPlugin);
        }

        if (inOption){
            Plugable inPlugin = new InstabilityLevelPlugin();
            core.analyze(inPlugin);
        }

        if (dsOption){
            Plugable dsPlugin = new MainSequenceDistancePlugin();
            core.analyze(dsPlugin);
        }
    }
}