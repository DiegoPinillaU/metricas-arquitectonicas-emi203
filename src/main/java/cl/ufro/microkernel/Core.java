package cl.ufro.microkernel;

import java.io.IOException;
import java.nio.file.Path;
import cl.ufro.plugins.Plugable;

public class Core{
    private ExplorationResultsFacade results;

    public void analyze(Plugable plugin){
        plugin.analyzeRepository(results);
    }

    public void startParsingRepository(Path folderPath){
        RepositoryExplorer explorer = new RepositoryExplorer();
        
        try {
            this.results = new ExplorationResultsFacade(explorer.exploreRepositoryClasses(folderPath));
        } catch (IOException e) {
            System.out.println("Imposible acceder al repositorio");
        }
    }
}
