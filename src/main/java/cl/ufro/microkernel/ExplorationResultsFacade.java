package cl.ufro.microkernel;

import java.util.List;

import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;

public class ExplorationResultsFacade {

    private List<ParseResult<CompilationUnit>> results;

    public ExplorationResultsFacade(List<ParseResult<CompilationUnit>> parseResults){
        this.results = parseResults;
    }

    public List<ParseResult<CompilationUnit>> getResults(){
        return this.results;
    }
    
}
