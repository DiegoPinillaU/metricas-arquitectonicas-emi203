package cl.ufro.microkernel;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RepositoryExplorer {
    public List<ParseResult<CompilationUnit>> exploreRepositoryClasses(Path folderPath) throws IOException{
        List<ParseResult<CompilationUnit>> result = new ArrayList<ParseResult<CompilationUnit>>();
        
        if (!isValidFolder(folderPath)){
            throw new IOException();
        }

        List<Path> javaSrcFilePaths = listJavaSrcFiles(folderPath);
        
        for (Path jsrcPath : javaSrcFilePaths) {
            result.add(new JavaParser().parse(new FileInputStream(jsrcPath.toString())));
        }

        return result;
    }

    private boolean isValidFolder(Path folderPath){
        File targetFolder = folderPath.toFile();
        if (targetFolder.exists() && targetFolder.isDirectory()) {
            return true;
        }
        System.out.println("Ruta al repositorio inválida: " + folderPath);
        return false;
    }

    private List<Path> listJavaSrcFiles(Path folderPath) throws IOException{
        List<Path> result;
        try (Stream<Path> walk = Files.walk(folderPath)){
            result = walk
                        .filter(Files::isRegularFile)
                        .filter(p -> p.getFileName().toString().endsWith(".java"))
                        .collect(Collectors.toList());
        }
        //System.out.println(result);
        return result;
    }
}
