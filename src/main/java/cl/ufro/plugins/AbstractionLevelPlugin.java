package cl.ufro.plugins;

import java.util.HashMap;
import java.util.Map;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;

import cl.ufro.microkernel.ExplorationResultsFacade;

public class AbstractionLevelPlugin implements Plugable{
    @Override
    public void analyzeRepository(ExplorationResultsFacade resultFacade) {
        System.out.println("Analizando el nivel de abstracción del repositorio");
        
        Map<String, PackageCounts> packageCountsMap = new HashMap<>();

        getCountsByPackage(resultFacade, packageCountsMap);

        printStatistics(packageCountsMap);
    }

    private void getCountsByPackage(ExplorationResultsFacade resultFacade,
            Map<String, PackageCounts> packageCountsMap) {
                resultFacade.getResults().forEach(
                    parseResult -> {
                        CompilationUnit unit = parseResult.getResult().get();
                        String currentPackage;

                        if (unit.getPackageDeclaration().isPresent()) {
                            currentPackage = unit.getPackageDeclaration().get().getNameAsString();
                        } else {
                            currentPackage = "Default";
                        }

                        PackageCounts packageCounts = packageCountsMap.computeIfAbsent(currentPackage, k -> new PackageCounts());

                        unit.findAll(ClassOrInterfaceDeclaration.class).forEach(
                            classOrInterfaceDeclaration -> {
                                if (classOrInterfaceDeclaration.isAbstract()) packageCounts.abstractClassCount++;
                                else if (classOrInterfaceDeclaration.isInterface()) packageCounts.interfaceCount++;
                                else packageCounts.concreteClassCount++;
                            }
                        );
                    }
                );
    }

    private void printStatistics(Map<String, PackageCounts> packageCountsMap){
        packageCountsMap.forEach(
            (packageName, packageCounts) -> {
                float ga = (float) (packageCounts.abstractClassCount + packageCounts.interfaceCount);
                ga /= (float) (packageCounts.abstractClassCount + packageCounts.interfaceCount + packageCounts.concreteClassCount);
                System.out.printf(
                    "Paquete %s:\n\t %d\tclases abstractas, %d\tinterfaces y %d\tclases concretas. GA = %f\n",
                    packageName, packageCounts.abstractClassCount, packageCounts.interfaceCount, packageCounts.concreteClassCount, ga);

            }
        );

    }

    private static class PackageCounts {
        public int interfaceCount = 0;
        public int abstractClassCount = 0;
        public int concreteClassCount = 0;
    }
}