package cl.ufro.plugins;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;

import cl.ufro.microkernel.ExplorationResultsFacade;

public class InstabilityLevelPlugin implements Plugable{

    @Override
    public void analyzeRepository(ExplorationResultsFacade resultsFacade) {
        System.out.println("Analizando el nivel de inestabilidad del repositorio");
        
        Map<String, PackageCounts> packageCountsMap = new HashMap<>();

        getCountsByPackage(resultsFacade, packageCountsMap);

        printStatistics(packageCountsMap);
    }

    private void getCountsByPackage(ExplorationResultsFacade resultsFacade,
            Map<String, PackageCounts> packageCountsMap) {
                resultsFacade.getResults().forEach(
                    parseResult -> {
                        CompilationUnit unit = parseResult.getResult().get();
                        String currentPackage;

                        if (unit.getPackageDeclaration().isPresent()) {
                            currentPackage = unit.getPackageDeclaration().get().getNameAsString();
                        } else {
                            currentPackage = "Default";
                        }

                        PackageCounts packageCounts = packageCountsMap.computeIfAbsent(currentPackage, k -> new PackageCounts());

                        unit.findAll(ImportDeclaration.class).forEach(
                            importDeclaration -> {
                                packageCounts.afferentCouplingCount++;
                            }
                        );

                        List<ClassOrInterfaceDeclaration> classesOrInterfacesDeclared = unit.findAll(ClassOrInterfaceDeclaration.class);

                        resultsFacade.getResults().forEach(
                            reParseResult -> {
                                CompilationUnit reUnit = reParseResult.getResult().get();
                                String rePackageName;
                                if (reUnit.getPackageDeclaration().isPresent()){
                                    rePackageName = reUnit.getPackageDeclaration().get().getNameAsString();
                                } else {
                                    rePackageName = "Default";
                                }

                                if (!reUnit.getPackageDeclaration().equals(unit.getPackageDeclaration()))
                                reUnit.findAll(ImportDeclaration.class).forEach(
                                    reImportDeclaration -> {
                                        classesOrInterfacesDeclared.forEach(
                                            cls -> {
                                                if (reImportDeclaration.getNameAsString().contains(cls.getNameAsString())) {
                                                    packageCounts.efferentCouplingCount++;
                                                    // System.out.printf("%s contiene %s en %s\n", reImportDeclaration.getNameAsString(), cls.getNameAsString(), rePackageName);
                                                }
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                );
    }

    private void printStatistics(Map<String, PackageCounts> packageCountsMap) {
        packageCountsMap.forEach(
            (packageName, packageCounts) -> {
                float in = (float) (packageCounts.efferentCouplingCount);
                in /= (float) (packageCounts.efferentCouplingCount + packageCounts.afferentCouplingCount);
                System.out.printf(
                    "Paquete %s:\n\tContiene %d\tdependencias y sus componentes son requeridos en %d\t otros. IN = %f\n",
                    packageName, packageCounts.afferentCouplingCount, packageCounts.efferentCouplingCount, in);

            }
        );
    }

    private static class PackageCounts {
        public int afferentCouplingCount = 0;
        public int efferentCouplingCount = 0;
    }

}
