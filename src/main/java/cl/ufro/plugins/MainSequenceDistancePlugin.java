package cl.ufro.plugins;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;

import cl.ufro.microkernel.ExplorationResultsFacade;

public class MainSequenceDistancePlugin implements Plugable{

    @Override
    public void analyzeRepository(ExplorationResultsFacade resultsFacade) {
        System.out.println("Analizando la distancia de la secuencia principal del repositorio");
        
        Map<String, PackageCounts> packageCountsMap = new HashMap<>();

        getCountsByPackage(resultsFacade, packageCountsMap);

        printStatistics(packageCountsMap);
    }

    private void getCountsByPackage(ExplorationResultsFacade resultsFacade,
            Map<String, PackageCounts> packageCountsMap) {
                resultsFacade.getResults().forEach(
                    parseResult -> {
                        CompilationUnit unit = parseResult.getResult().get();
                        String currentPackage;

                        if (unit.getPackageDeclaration().isPresent()) {
                            currentPackage = unit.getPackageDeclaration().get().getNameAsString();
                        } else {
                            currentPackage = "Default";
                        }

                        PackageCounts packageCounts = packageCountsMap.computeIfAbsent(currentPackage, k -> new PackageCounts());

                        unit.findAll(ImportDeclaration.class).forEach(
                            importDeclaration -> {
                                packageCounts.afferentCouplingCount++;
                            }
                        );

                        unit.findAll(ClassOrInterfaceDeclaration.class).forEach(
                            classOrInterfaceDeclaration -> {
                                if (classOrInterfaceDeclaration.isAbstract()) packageCounts.abstractClassCount++;
                                else if (classOrInterfaceDeclaration.isInterface()) packageCounts.interfaceCount++;
                                else packageCounts.concreteClassCount++;
                            }
                        );

                        List<ClassOrInterfaceDeclaration> classesOrInterfacesDeclared = unit.findAll(ClassOrInterfaceDeclaration.class);

                        resultsFacade.getResults().forEach(
                            reParseResult -> {
                                CompilationUnit reUnit = reParseResult.getResult().get();
                                String rePackageName;
                                if (reUnit.getPackageDeclaration().isPresent()){
                                    rePackageName = reUnit.getPackageDeclaration().get().getNameAsString();
                                } else {
                                    rePackageName = "Default";
                                }

                                if (!reUnit.getPackageDeclaration().equals(unit.getPackageDeclaration()))
                                reUnit.findAll(ImportDeclaration.class).forEach(
                                    reImportDeclaration -> {
                                        classesOrInterfacesDeclared.forEach(
                                            cls -> {
                                                if (reImportDeclaration.getNameAsString().contains(cls.getNameAsString())) {
                                                    packageCounts.efferentCouplingCount++;
                                                    // System.out.printf("%s contiene %s en %s\n", reImportDeclaration.getNameAsString(), cls.getNameAsString(), rePackageName);
                                                }
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                );
    }

    private void printStatistics(Map<String, PackageCounts> packageCountsMap) {
        packageCountsMap.forEach(
            (packageName, packageCounts) -> {
                float in = (float) (packageCounts.efferentCouplingCount) / (float) (packageCounts.efferentCouplingCount + packageCounts.afferentCouplingCount);
                float ga = (float) (packageCounts.abstractClassCount + packageCounts.interfaceCount) / (float) (packageCounts.abstractClassCount + packageCounts.interfaceCount + packageCounts.concreteClassCount);
                float ds = Math.abs(ga + in - 1);

                System.out.printf("Paquete %s:\n", packageName);
                
                System.out.printf(
                    "\t * GA = %f ", ga
                );
                    
                System.out.printf(
                    "\t * IN = %f", in
                );

                System.out.printf(
                    "\t * DS = | GA + IN - 1 | = %f", ds
                );
                System.out.println();
            }
        );

    }

    private static class PackageCounts {
        public int afferentCouplingCount = 0;
        public int efferentCouplingCount = 0;
        public int interfaceCount = 0;
        public int abstractClassCount = 0;
        public int concreteClassCount = 0;
    }
}
