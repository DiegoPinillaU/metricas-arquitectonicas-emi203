package cl.ufro.plugins;

import cl.ufro.microkernel.ExplorationResultsFacade;

public interface Plugable {
    public void analyzeRepository(ExplorationResultsFacade results);
}
